/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seari.seiec104.client;

import org.openmuc.j60870.ASdu;

/**
 *
 * @author Rainbow
 */
public interface BaseDataProcessor 
{
    public void processData(ASdu aSdu);
    public void setProcessorName(String name);
    public String getProcessorName();
    public void setReady(boolean isReady);
    public void setIecClient(IEC104BaseClient client);
}
