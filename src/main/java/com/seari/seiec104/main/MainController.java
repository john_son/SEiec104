/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seari.seiec104.main;

import com.seari.seiec104.bean.Iec104Device;
import com.seari.seiec104.client.BaseDataProcessor;
import com.seari.seiec104.client.IEC104BaseClient;
import com.seari.seiec104.client.RedisProcessor;
import com.seari.seiec104.utils.DeviceUtil;
import com.seari.seiec104.utils.MiscTools;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Rainbow
 */
public class MainController
{
    protected static Logger logger = LoggerFactory.getLogger(MainController.class);
    static List<Iec104Device> devices = new ArrayList<>();
    public static Properties config;
    static String configFilePath = "config.cfg";
    public static void main(String[] args) throws InterruptedException
    {
        //args = new String[]{"127.0.0.1","2404"};
        MainController controller = new MainController();
        controller.init();
        if(config.getProperty("use_config").equals("1"))
        {
            controller.initWithConfigFile();
        }
        IecWorker worker = new IecWorker(devices);
        new Thread(worker).start();
    }
    
    public void init()
    {
        try
        {
            //System.out.println(System.getProperty("user.dir"));
            config = MiscTools.loadConfig(configFilePath);
        } catch (Exception ex)
        {
            logger.warn(ex.getMessage());
        }
        logger.info("read config file complete");
    }
    
    private void initWithConfigFile()
    {
        String configList = config.getProperty("device_list");
        String[] lists = configList.split("\\|");
        for (int i = 0; i < lists.length; i++)
        {
            String deviceStr = lists[i];
            String[] strs = deviceStr.split(":");
            if (null != strs && strs[0].equals(DeviceUtil.IEC104))
            {
                //处理真实设备配置以供采集
                Iec104Device device = new Iec104Device();
                device.setIp(config.getProperty(strs[1] + "_ip"));
                device.setPort(Integer.parseInt(config.getProperty(strs[1] + "_port")));
                device.setStationID(Integer.parseInt(config.getProperty(strs[1] + "_stationID")));
                device.setDeviceName(config.getProperty(strs[1] + "_name"));
                if("1".equals(config.getProperty(strs[1] + "_hasCounter")))
                {
                    device.setHasCounter(true);
                }
                devices.add(device);
            }
        }
    }
}
