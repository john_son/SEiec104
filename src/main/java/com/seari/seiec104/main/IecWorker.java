/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seari.seiec104.main;

import com.seari.seiec104.bean.Iec104Device;
import com.seari.seiec104.client.BaseDataProcessor;
import com.seari.seiec104.client.IEC104BaseClient;
import com.seari.seiec104.client.RedisProcessor;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Rainbow
 */
public class IecWorker implements Runnable
{
    protected static Logger logger = LoggerFactory.getLogger(IecWorker.class);
    List<Iec104Device> devices;
    static ExecutorService executorService = Executors.newCachedThreadPool();
    
    public IecWorker(List<Iec104Device> devices)
    {
        this.devices = devices;
    }
    
    @Override
    public void run()
    {
        for (Iec104Device device : devices)
        {
            IEC104BaseClient client = new IEC104BaseClient(device);
            BaseDataProcessor processor = new RedisProcessor();
            processor.setProcessorName(device.getDeviceName());
            processor.setIecClient(client);
            client.setProcessor(processor);
            client.setupClient();
            executorService.submit((RedisProcessor)processor);
        }
    }
    
}
